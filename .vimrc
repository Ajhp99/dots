" ~/.vimrc
" Ajhp

" Load Pathogen
execute pathogen#infect()
execute pathogen#helptags()

filetype plugin indent on

" Settings
filetype plugin on
filetype off
syntax on

set noerrorbells
set visualbell
set t_vb=

set sts=2
set sw=2
set ts=2
set et

set nowrap
set cc=75
set nu
set ai

set nocp
set sc

set noswapfile
set nobackup

set encoding=utf8
set history=1000

set ttyfast

" colors
set tgc
set bg=dark

colorscheme Tomorrow-Night 

" pymode
let g:pymode_python = 'python3'

" indentLine
let g:indentLine_enabled = 1
let g:indentLine_char = '¦'
let g:indentLine_first_char = '¦'
let g:indentLine_showFirstIndentLevel = 1

"javascript
let g:javascript_plugin_flow = 1
let g:javascript_plugin_jsdoc = 1

" syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" ale
let g:ale_fix_on_save = 1
let g:ale_sign_column_always = 0
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'

let g:ale_fixers = {
    \ 'javascript': ['prettier', 'eslint']
    \ }

" nerdtree
let g:NERDTreeWinSize = 25
let g:NERDTreeMinimalUI = 1
let g:NERDTreeShowHidden = 1
let g:NERDTreeShowBookmarks = 1

" gitgutter
let g:gitgutter_sign_added = '+'
let g:gitgutter_sign_modified = '*'
let g:gitgutter_sign_removed = '-'
let g:gitgutter_sign_removed_first_line = 'x'
let g:gitgutter_sign_modified_removed = 'x'

" Fix POSIX compliance
if &shell =~# 'fish$'
    set shell=sh
endif

" Augroups
augroup javascript_folding
    au!
    au FileType javascript setlocal foldmethod=syntax
augroup END

augroup indents
    au!
    au FileType less,css,html setlocal ts=2 sts=2 sw=2 expandtab
    au FileType text,markdown setlocal expandtab
    au FileType python setlocal ts=4 sts=4 sw=4 expandtab
augroup END

" Mappings
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

nmap s <Plug>(easymotion-overwin-f2)

map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)

map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

map <C-r> :source ~/.vimrc<CR>
map <C-f> :NERDTreeToggle<CR>
map <C-t> :tabnew<CR>
map <C-p> :tabprev<CR>
map <C-n> :tabnext<CR>
map <C-g> :Goyo<CR>
