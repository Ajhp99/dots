#!/bin/sh

# Kill polybar
killall -q polybar

# Wait till polybar shuts down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Start bar
polybar foo

# Notification
notify-send "Bar launched..."
